﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCMovement : MonoBehaviour
{
    public float time;
    public float range;

    public Transform[] pathPoints;

    int index;
    float speed;
    float agentSpeed;
    Transform player;

    Animator animate;
    NavMeshAgent agent;



    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();

        if(agent != null)
        { 
            agentSpeed = agent.speed; 
        }

        player = GameObject.Find("Player").transform;
        index = Random.Range(0, pathPoints.Length);

        InvokeRepeating("Tick", 0, 0.5f);

        if(pathPoints.Length > 0)
        {
            InvokeRepeating("Patrol", 0, time);
        }
    }

    void Patrol()
    {
        index = index == pathPoints.Length - 1 ? 0 : index + 1;
    }

    void Tick()
    {
        agent.destination = pathPoints[index].position;
        agent.speed = agentSpeed / 2;

        if(player != null & Vector3.Distance(transform.position, player.position) < range)
        {
            agent.destination = player.position;
            agent.speed = agentSpeed;
        }

    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
